from django.shortcuts import render, redirect
from .models import Schedule
from . import forms
from django.http import HttpResponse


# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def education(request):
    return render(request, 'education.html')

def experience(request):
    return render(request, 'experience.html')

def skills(request):
    return render(request, 'skills.html')

def schedule1(request):
    schedules = Schedule.objects.all().order_by('date')
    return render(request, 'schedule.html', {'schedules': schedules})

def create_schedule(request):
    if request.method == 'POST':
        form = forms.ScheduleForm(request.POST)
        print(request.POST)
        if form.is_valid():
            form.save()
            print(Schedule.objects.all())
            return redirect('homepage:schedule')
    else:
        form = forms.ScheduleForm()
    return render(request, 'create_schedule.html', {'form': form})

def schedule_delete(request):
	Schedule.objects.all().delete()
	return redirect('homepage:schedule')

def contact(request):
    return render(request, 'contact.html')
