from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('education/', views.education, name='education'),
    path('experience/', views.experience, name='experience'),
    path('skills/', views.skills, name='skills'),
    path('schedule/', views.schedule1, name='schedule'),
    path('create_schedule/', views.create_schedule, name='create_schedule'),
    path('schedule_delete/', views.schedule_delete, name='schedule_delete'),
    path('contact/', views.contact, name='contact'),
]
