from django.db import models

class Jadwal(models.Model):
	DAY_CHOICES = (
		('Mon', 'Monday'),
		('Tue','Tuesday'),
		('Wed','Wednesday'),
		('Thu','Thursday'),
		('Fri','Friday'),
		('Sat','Saturday'),
		('Sun','Sunday')
		)

	day = models.CharField(choices=DAY_CHOICES, max_length=20)
	date = models.DateField()
	time = models.TimeField()
	name = models.CharField(max_length=30)
	location = models.CharField(max_length=30)
	category = models.CharField(max_length=20)